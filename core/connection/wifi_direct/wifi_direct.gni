# Copyright (c) 2023 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("//foundation/communication/dsoftbus/dsoftbus.gni")

native_source_path = rebase_path("$dsoftbus_root_path")
wifi_direct_enhance_dir = "dsoftbus_enhance/core/connection/wifi_direct"
enhanced_wifi_direct = exec_script("$dsoftbus_root_path/check_sub_module.py",
                                   [
                                     "$native_source_path",
                                     "$wifi_direct_enhance_dir",
                                   ],
                                   "value")

wifi_direct_connection_src = []
wifi_direct_connection_external_deps = []

wifi_direct_connection_inc = [
  "$dsoftbus_root_path/core/connection/wifi_direct",
  "//foundation/communication/wifi/wifi/interfaces/kits/c",
  "$dsoftbus_root_path/core/connection/wifi_direct/adapter/single",
]

if (dsoftbus_feature_conn_p2p == true &&
    softbus_communication_wifi_feature == true) {
  wifi_direct_connection_src += [
    "$dsoftbus_root_path/core/connection/wifi_direct/wifi_direct_coexist_rule.c",
    "$dsoftbus_root_path/core/connection/wifi_direct/wifi_direct_command_manager.c",
    "$dsoftbus_root_path/core/connection/wifi_direct/wifi_direct_decision_center.c",
    "$dsoftbus_root_path/core/connection/wifi_direct/wifi_direct_initiator.c",
    "$dsoftbus_root_path/core/connection/wifi_direct/wifi_direct_manager.c",
    "$dsoftbus_root_path/core/connection/wifi_direct/wifi_direct_ip_manager.c",
    "$dsoftbus_root_path/core/connection/wifi_direct/wifi_direct_negotiator.c",
    "$dsoftbus_root_path/core/connection/wifi_direct/wifi_direct_role_negotiator.c",
    "$dsoftbus_root_path/core/connection/wifi_direct/channel/default_negotiate_channel.c",
    "$dsoftbus_root_path/core/connection/wifi_direct/channel/fast_connect_negotiate_channel.c",
    "$dsoftbus_root_path/core/connection/wifi_direct/data/info_container.c",
    "$dsoftbus_root_path/core/connection/wifi_direct/data/inner_link.c",
    "$dsoftbus_root_path/core/connection/wifi_direct/data/wifi_config_info.c",
    "$dsoftbus_root_path/core/connection/wifi_direct/data/interface_info.c",
    "$dsoftbus_root_path/core/connection/wifi_direct/data/link_info.c",
    "$dsoftbus_root_path/core/connection/wifi_direct/data/link_manager.c",
    "$dsoftbus_root_path/core/connection/wifi_direct/data/negotiate_message.c",
    "$dsoftbus_root_path/core/connection/wifi_direct/data/resource_manager.c",
    "$dsoftbus_root_path/core/connection/wifi_direct/data/wifi_direct_intent.c",
    "$dsoftbus_root_path/core/connection/wifi_direct/entity/p2p_entity/p2p_entity.c",
    "$dsoftbus_root_path/core/connection/wifi_direct/entity/p2p_entity/p2p_entity_state.c",
    "$dsoftbus_root_path/core/connection/wifi_direct/entity/p2p_entity/p2p_available_state.c",
    "$dsoftbus_root_path/core/connection/wifi_direct/entity/p2p_entity/p2p_unavailable_state.c",
    "$dsoftbus_root_path/core/connection/wifi_direct/entity/p2p_entity/p2p_group_connecting_state.c",
    "$dsoftbus_root_path/core/connection/wifi_direct/entity/p2p_entity/p2p_group_creating_state.c",
    "$dsoftbus_root_path/core/connection/wifi_direct/entity/p2p_entity/p2p_group_removing_state.c",
    "$dsoftbus_root_path/core/connection/wifi_direct/negotiate_state/available_state.c",
    "$dsoftbus_root_path/core/connection/wifi_direct/negotiate_state/processing_state.c",
    "$dsoftbus_root_path/core/connection/wifi_direct/negotiate_state/waiting_connect_request_state.c",
    "$dsoftbus_root_path/core/connection/wifi_direct/negotiate_state/waiting_connect_response_state.c",
    "$dsoftbus_root_path/core/connection/wifi_direct/processor/p2p_v1_processor.c",
    "$dsoftbus_root_path/core/connection/wifi_direct/processor/p2p_v2_processor.c",
    "$dsoftbus_root_path/core/connection/wifi_direct/protocol/json_protocol.c",
    "$dsoftbus_root_path/core/connection/wifi_direct/protocol/tlv_protocol.c",
    "$dsoftbus_root_path/core/connection/wifi_direct/protocol/wifi_direct_protocol_factory.c",
    "$dsoftbus_root_path/core/connection/wifi_direct/utils/wifi_direct_ipv4_info.c",
    "$dsoftbus_root_path/core/connection/wifi_direct/utils/wifi_direct_network_utils.c",
    "$dsoftbus_root_path/core/connection/wifi_direct/utils/wifi_direct_timer_list.c",
    "$dsoftbus_root_path/core/connection/wifi_direct/utils/wifi_direct_work_queue.c",
    "$dsoftbus_root_path/core/connection/wifi_direct/utils/wifi_direct_utils.c",
    "$dsoftbus_root_path/core/connection/wifi_direct/utils/wifi_direct_anonymous.c",
    "$dsoftbus_root_path/core/connection/wifi_direct/utils/wifi_direct_perf_recorder.c",
    "$dsoftbus_root_path/core/connection/wifi_direct/processor/wifi_direct_processor_factory.c",
    "$dsoftbus_root_path/core/connection/wifi_direct/entity/wifi_direct_entity_factory.c",
    "$dsoftbus_root_path/core/connection/wifi_direct/adapter/single/broadcast_handler.c",
    "$dsoftbus_root_path/core/connection/wifi_direct/adapter/single/broadcast_receiver.c",
    "$dsoftbus_root_path/core/connection/wifi_direct/adapter/single/link_manager_broadcast_handler.c",
    "$dsoftbus_root_path/core/connection/wifi_direct/adapter/single/p2p_entity_broadcast_handler.c",
    "$dsoftbus_root_path/core/connection/wifi_direct/adapter/single/resource_manager_broadcast_handler.c",
    "$dsoftbus_root_path/core/connection/wifi_direct/adapter/single/wifi_direct_p2p_adapter.c",
  ]

  if (enhanced_wifi_direct) {
    import(
        "//foundation/communication/dsoftbus/dsoftbus_enhance/core/connection/wifi_direct/wifi_direct.gni")

    wifi_direct_connection_inc += wifi_direct_connection_enhance_inc
    wifi_direct_connection_src += wifi_direct_connection_enhance_src
  } else {
    wifi_direct_connection_src += []
  }

  wifi_direct_connection_external_deps += [ "wifi:wifi_sdk" ]
} else {
  wifi_direct_connection_src += [
    "$dsoftbus_root_path/core/connection/wifi_direct/wifi_direct_initiator_virtual.c",
    "$dsoftbus_root_path/core/connection/wifi_direct/wifi_direct_manager_virtual.c",
    "$dsoftbus_root_path/core/connection/wifi_direct/channel/default_negotiate_channel_virtual.c",
    "$dsoftbus_root_path/core/connection/wifi_direct/channel/fast_connect_negotiate_channel_virtual.c",
  ]
}
